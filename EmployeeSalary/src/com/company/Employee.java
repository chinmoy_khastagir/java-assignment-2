package com.company;

class Employee {
    private String employeeName;
    private int employeeGrade;
    private String employeeAddress;
    private String employeeMobileNo;
    private String accountType;
    private String accountName;
    private String accountNumber;
    private long employeeCurrentBalance;
    private String bankName;
    private String bankBranchName;
    private long employeeSalary;

    Employee(String name, int grade, long salary, String address, String mobileNo, String accType, String accName, String accNum, String bank, String branch ){
        employeeName = name;
        employeeGrade = grade;
        employeeSalary = salary;
        employeeAddress = address;
        employeeMobileNo = mobileNo;
        accountType = accType;
        accountName = accName;
        accountNumber = accNum;
        bankName = bank;
        bankBranchName = branch;
    }

    void setEmployeeCurrentBalance(long employeeCurrentBalance){
        this.employeeCurrentBalance = employeeCurrentBalance;
    }

    void  printEmployeeInfo(){
        System.out.print("Employee Name: "+employeeName+"\n");
        System.out.print("Employee Rank: "+employeeGrade+"\n");
        System.out.print("Employee Salary: "+employeeSalary+"\n\n");
        System.out.print("------------------------------------------------\n\n");
    }
}
