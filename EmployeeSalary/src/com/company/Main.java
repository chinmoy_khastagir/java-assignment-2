package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here

        long basicOflowestgrade, balanceCompany;

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter Basic salary(in Tk) of the lowest grade employee: ");
        basicOflowestgrade = sc.nextLong();
        System.out.println("Enter Balance(in Tk) of company bank account: ");
        balanceCompany = sc.nextLong();

        Company.currentBalance = balanceCompany;
        Company.basicSalarayGrade1 = basicOflowestgrade;

        Company alexa = new Company();
        alexa.paySalary();

        System.out.print("\n\nSalary is paid completely.\n\n");
        System.out.print("Employee Info -->");
        System.out.print("\n-------------------\n\n");

        for(int i=0; i<10; i++){
            Company.employeeList[i].printEmployeeInfo();
        }

        System.out.print("Company Info -->");
        System.out.print("\n-------------------\n\n");
        System.out.print("Total Salary Paid by Company: "+Company.paidSalary+" tk\n");
        System.out.print("Remaining balance in Company Account: "+Company.currentBalance+" tk\n");





    }
}
