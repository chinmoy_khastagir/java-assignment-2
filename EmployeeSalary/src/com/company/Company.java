package com.company;

import java.util.Scanner;

class Company {

    static long currentBalance;
    static  long paidSalary;
    static long basicSalarayGrade1;
    static int[] employeeNumArray ={1,1,2,2,2,2};
    static long[] salaryArray = new long[10];
    static Employee[] employeeList = new Employee[10];
    private long basicSalary;
    private long rent;
    private long medicalAllowance;


    void setSalary(){

        long totalSalary;
        int k=0;
        basicSalary = basicSalarayGrade1;

        for(int i=0;i<6;i++){

            rent = ((basicSalary*20)/100);
            medicalAllowance = ((basicSalary*15)/100);
            totalSalary = basicSalary + rent + medicalAllowance;

            for(int j=0;j<employeeNumArray[i];j++){
                salaryArray[k] = totalSalary;
                String name = "Employee"+(k+1);
                Employee e = new Employee(name, i+1, totalSalary, "CTG", "01***", "Current", name, "9***", "HSBC", "GEC");
                employeeList[k] = e;
                k++;
            }

            basicSalary = basicSalary + 5000;
        }
    }



    void paySalary(){

        long newMoney;
        setSalary();

        for(int i=0;i<10;i++){
            if(currentBalance<salaryArray[i]){
                do{
                    System.out.println("Current balance is "+(salaryArray[i]-currentBalance)+"taka lower than the salary to be paid next.");
                    System.out.println("Salary to be paid next: "+salaryArray[i]+" tk.   Current Balance: "+currentBalance+" tk.");
                    System.out.println("Enter money into company account: ");
                    Scanner sc = new Scanner(System.in);
                    newMoney = sc.nextLong();
                    currentBalance = currentBalance + newMoney;
                }while (currentBalance<salaryArray[i]);
            }
            else{
                employeeList[i].setEmployeeCurrentBalance(salaryArray[i]);
                paidSalary = paidSalary + salaryArray[i];
                currentBalance = currentBalance - salaryArray[i];
            }

        }
    }

}
