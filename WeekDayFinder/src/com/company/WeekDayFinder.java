package com.company;

import java.util.Scanner;

public class WeekDayFinder{

    public static void main(String[] args) {

        int dayNumber;
        String[] weekDayArray = {"Saturday", "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday"};

        do{

            System.out.print("Enter the day number of the month(1-31)/0 to exit program: ");
            Scanner scan = new Scanner(System.in);
            dayNumber = scan.nextInt();
            if(dayNumber < 0 || dayNumber > 31){
                System.out.print("Day Number must be between 1 and 31. Please give input accordingly.\n");
                continue;
            }
            if(dayNumber!=0){
                System.out.println(weekDayArray[dayNumber%7]);
            }

        }while(dayNumber!=0);

    }
}
